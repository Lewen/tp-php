# Installer le projet

## Créer la base de données
- Démarrer votre serveur de base de données MySQL / MariaDB
- Modifier la ligne DATABASE_URL du fichier .env pour indiquer le user/password de votre serveur
- Exécuter la commande suivante pour créer la base de données :
`php bin/console doctrine:database:create`
- Exécuter la commande suivante pour créer les tables :
`php bin/console doctrine:schema:create`

## DataFixtures
Afin de pouvoir utiliser l'application, il faut ajouter des données de tests, notamment celles des utilisateurs (admin...), pour cela lancer la commande suivante :
`php bin/console doctrine:fixtures:load`

## Comptes utilisateurs
Tous les mots de passe sont : password

Pour les noms d'utilisateur on a :
### Admin
admin

### Collaborator
collaborator
johnsnow

### Commercial
arthur

### Freelance
victorien

# DEV
## SASS
Lancer la commande "composer run-sass" pour prendre en compte les modifs SASS