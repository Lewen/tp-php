<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210624104136 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE person_skill (id INT AUTO_INCREMENT NOT NULL, skill_id INT DEFAULT NULL, person_id INT DEFAULT NULL, level INT DEFAULT NULL, favorite TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_F20BFBB35585C142 (skill_id), INDEX IDX_F20BFBB3217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE person_skill ADD CONSTRAINT FK_F20BFBB35585C142 FOREIGN KEY (skill_id) REFERENCES skill (id)');
        $this->addSql('ALTER TABLE person_skill ADD CONSTRAINT FK_F20BFBB3217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('DROP TABLE profile_skill');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE profile_skill (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, level INT DEFAULT NULL, favorite TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE person_skill');
    }
}
