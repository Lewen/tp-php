<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *  "admin" = "Administrator",
 *  "collaborator" = "Collaborator",
 *  "freelance" = "Freelance",
 *  "commercial" = "Commercial",
 *  "candidate" = "Candidate",
 *  "user" = "User"
 * })
 */
abstract class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string",
     *  length=255, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $job;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $digitalKnowledge;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $webUsing;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $objective;

    /**
     * One Person may have Many PersonSkill
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PersonSkill", mappedBy="person", cascade={"persist", "remove"})
     */
    private $personSkills;


    public function __construct()
    {
        $this->personSkills = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(?string $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getDigitalKnowledge(): ?string
    {
        return $this->digitalKnowledge;
    }

    public function setDigitalKnowledge(?string $digitalKnowledge): self
    {
        $this->digitalKnowledge = $digitalKnowledge;

        return $this;
    }

    public function getWebUsing(): ?string
    {
        return $this->webUsing;
    }

    public function setWebUsing(?string $webUsing): self
    {
        $this->webUsing = $webUsing;

        return $this;
    }

    public function getObjective(): ?string
    {
        return $this->objective;
    }

    public function setObjective(?string $objective): self
    {
        $this->objective = $objective;

        return $this;
    }

    public function getPersonSkills(): Collection
    {
        return $this->personSkills;
    }

    public function setPersonSkills(?Collection $personSkills): self
    {
        if (is_null($personSkills)) {
            $personSkills = new ArrayCollection();
        }

        $this->personSkills = $personSkills;

        return $this;
    }

    public function addPersonSkill(PersonSkill $personSkill): self
    {
        $personSkill->setPerson($this);

        if (!$this->personSkills->contains($personSkill)) {
            $this->personSkills->add($personSkill);
        }

        return $this;
    }

    public function removePersonSkill(PersonSkill $personSkill): self
    {
        $this->personSkills->removeElement($personSkill);

        return $this;
    }
}
