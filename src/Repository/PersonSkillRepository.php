<?php

namespace App\Repository;

use App\Entity\PersonSkill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PersonSkill|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonSkill|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonSkill[]    findAll()
 * @method PersonSkill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonSkillRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonSkill::class);
    }

    // /**
    //  * @return PersonSkill[] Returns an array of PersonSkill objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PersonSkill
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
