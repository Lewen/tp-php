<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="profile")
     */
    public function index(Request $request): Response
    {
        $currentUser = $this->getUser();
        $formUser = $this->createForm(UserType::class, $currentUser);

        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($currentUser);
            $entityManager->flush();
            $this->addFlash("success", "Vos informations ont bien été mises à jour.");

            return $this->redirectToRoute('profile');
        }

        return $this->render('profile/profile.html.twig', [
            'controller_name' => 'ProfileController',
            'user' => $currentUser,
            'formUser' => $formUser->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="profile_edit", methods={"GET"})
     */
    public function edit(Request $request, Person $person): Response
    {
        $formUser = $this->createForm(UserType::class, $person);

        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($person);
            $entityManager->flush();
            $this->addFlash("success", "Vos informations ont bien été mises à jour.");

            return $this->redirectToRoute('profile');
        }

        return $this->render('profile/profile.html.twig', [
            'controller_name' => 'ProfileController',
            'user' => $person,
            'formUser' => $formUser->createView()
        ]);
    }
}
