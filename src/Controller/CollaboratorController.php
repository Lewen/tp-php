<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CollaboratorController extends AbstractController
{
    /**
     * @Route("/collaborator", name="collaborator")
     */
    public function index(): Response
    {
        return $this->render('collaborator/index.html.twig', [
            'controller_name' => 'CollaboratorController',
        ]);
    }
}
