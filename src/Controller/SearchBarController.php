<?php

namespace App\Controller;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchBarController extends AbstractController
{

    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request, Connection $connection)
    {
        $keywords = $request->get('keywords');

        $results = $this->search($keywords, $connection);

        return $this->render('search.html.twig', [
            'controller_name' => 'SearchBarController',
            'results' => $results
        ]);
    }

    public function search($keywords, $connection)
    {
        $sql = '
            SELECT *
            FROM person p
            LEFT JOIN person_skill ps
                ON p.id = ps.person_id
            LEFT JOIN skill sk
                ON ps.skill_id = sk.id
            WHERE firstname LIKE :keywords
            GROUP BY p.id
            ORDER BY p.firstname ASC
        ';

        $stmt = $connection->prepare($sql);
        $stmt->executeQuery(['keywords' => "%$keywords%"]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAllAssociative();
    }
}
