<?php

namespace App\DataFixtures;

use App\Entity\Candidate;
use App\Entity\Category;
use App\Entity\Collaborator;
use App\Entity\Commercial;
use App\Entity\Company;
use App\Entity\Freelance;
use App\Entity\PersonSkill;
use App\Entity\Skill;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setFirstname('Admin');
        $userAdmin->setLastname('Istrator');

        $userAdmin->setUsername('admin');
        $password = $this->encoder->hashPassword($userAdmin, 'password');
        $userAdmin->setPassword($password);
        $userAdmin->addRole(User::ROLE_ADMIN);

        $manager->persist($userAdmin);


        // -- SKILLS --
        $category = new Category();
        $category->setName('Savoir-Être');

        $manager->persist($category);
        $manager->flush();

        $skill1 = new Skill();
        $skill1->setName('Cool');
        $skill1->setCategory($category);

        $manager->persist($skill1);

        $skill2 = new Skill();
        $skill2->setName('Gentil');
        $skill2->setCategory($category);

        $manager->persist($skill2);

        $skill3 = new Skill();
        $skill3->setName('Volontaire');
        $skill3->setCategory($category);

        $manager->persist($skill3);

        $category = new Category();
        $category->setName('Savoir-Faire');

        $manager->persist($category);
        $manager->flush();

        $skill4 = new Skill();
        $skill4->setName('Cuisine');
        $skill4->setCategory($category);

        $manager->persist($skill4);

        $skill5 = new Skill();
        $skill5->setName('Dev Front');
        $skill5->setCategory($category);

        $manager->persist($skill5);

        $skill6 = new Skill();
        $skill6->setName('Dev Back');
        $skill6->setCategory($category);

        $manager->persist($skill6);

        $skill7 = new Skill();
        $skill7->setName('SAV');
        $skill7->setCategory($category);

        $manager->persist($skill7);

        $manager->flush();

        $personSkill1 = new PersonSkill();
        $personSkill1->setSkill($skill1);
        $personSkill2 = new PersonSkill();
        $personSkill2->setSkill($skill2);
        $personSkill3 = new PersonSkill();
        $personSkill3->setSkill($skill3);
        $personSkill4 = new PersonSkill();
        $personSkill4->setSkill($skill4);
        $personSkill5 = new PersonSkill();
        $personSkill5->setSkill($skill5);
        $personSkill6 = new PersonSkill();
        $personSkill6->setSkill($skill6);
        $personSkill7 = new PersonSkill();
        $personSkill7->setSkill($skill7);
        $personSkill8 = new PersonSkill();
        $personSkill8->setSkill($skill1);
        $personSkill9 = new PersonSkill();
        $personSkill9->setSkill($skill2);
        $personSkill10 = new PersonSkill();
        $personSkill10->setSkill($skill3);
        $personSkill11 = new PersonSkill();
        $personSkill11->setSkill($skill4);
        $personSkill12 = new PersonSkill();
        $personSkill12->setSkill($skill5);
        $personSkill13 = new PersonSkill();
        $personSkill13->setSkill($skill6);
        $personSkill14 = new PersonSkill();
        $personSkill14->setSkill($skill7);

        $manager->persist($personSkill1);
        $manager->persist($personSkill2);
        $manager->persist($personSkill3);
        $manager->persist($personSkill4);
        $manager->persist($personSkill5);
        $manager->persist($personSkill6);
        $manager->persist($personSkill7);
        $manager->persist($personSkill8);
        $manager->persist($personSkill9);
        $manager->persist($personSkill10);
        $manager->persist($personSkill11);
        $manager->persist($personSkill12);
        $manager->persist($personSkill13);
        $manager->persist($personSkill14);

        $manager->flush();

        // -- / SKILLS --

        // -- COLLABORATORS
        // Collab 1
        $userCollaborator = new Collaborator();
        $userCollaborator->setFirstname('Colla');
        $userCollaborator->setLastname('Borator');
        $userCollaborator->setEmail('collab@borator.fr');
        $userCollaborator->setTel('06 75 48 83 49');
        $userCollaborator->setBirthDate(new \DateTime('1967-01-08'));
        $userCollaborator->setJob('Assistant');
        $userCollaborator->setDigitalKnowledge('Moyenne');
        $userCollaborator->setWebUsing('Suivi des réseaux sociaux, veille, achat en ligne… principalement sur mobile');
        $userCollaborator->setObjective('Créer son profil');
        $userCollaborator->addPersonSkill($personSkill1);
        $userCollaborator->addPersonSkill($personSkill2);

        $userCollaborator->setUsername('collaborator');
        $password = $this->encoder->hashPassword($userCollaborator, 'password');
        $userCollaborator->setPassword($password);
        $userCollaborator->addRole(User::ROLE_COLLABORATOR);

        $manager->persist($userCollaborator);

        // Collab 2
        $userCollaborator = new Collaborator();
        $userCollaborator->setFirstname('John');
        $userCollaborator->setLastname('Snow');
        $userCollaborator->setEmail('john@snow.fr');
        $userCollaborator->setTel('06 75 48 83 49');
        $userCollaborator->setBirthDate(new \DateTime('1970-11-18'));
        $userCollaborator->setJob('Dev Front');
        $userCollaborator->setDigitalKnowledge('Très bonne');
        $userCollaborator->setWebUsing('Suivi des réseaux sociaux, veille, achat en ligne… principalement sur mobile');
        $userCollaborator->setObjective('Créer son profil');
        $userCollaborator->addPersonSkill($personSkill3);
        $userCollaborator->addPersonSkill($personSkill4);
        $userCollaborator->addPersonSkill($personSkill5);

        $userCollaborator->setUsername('johnsnow');
        $password = $this->encoder->hashPassword($userCollaborator, 'password');
        $userCollaborator->setPassword($password);
        $userCollaborator->addRole(User::ROLE_COLLABORATOR);

        $manager->persist($userCollaborator);

        // -- / COLLABORATORS

        // -- COMMERCIALS
        $userCommercial = new Commercial();
        $userCommercial->setFirstname('Arthur');
        $userCommercial->setLastname('LE GRAND');
        $userCommercial->setEmail('arthur@legrand.fr');
        $userCommercial->setTel('06 75 48 83 88');
        $userCommercial->setBirthDate(new \DateTime('1965-01-08'));
        $userCommercial->setJob('Dev Front');
        $userCommercial->setDigitalKnowledge('Bonne');
        $userCommercial->setWebUsing('Suivi des réseaux sociaux, veille, achat en ligne… principalement sur mobile');
        $userCommercial->setObjective('Créer son profil de fou');
        $userCommercial->addPersonSkill($personSkill6);
        $userCommercial->addPersonSkill($personSkill7);

        $userCommercial->setUsername('arthur');
        $password = $this->encoder->hashPassword($userCommercial, 'password');
        $userCommercial->setPassword($password);
        $userCommercial->addRole(User::ROLE_COMMERCIAL);

        $manager->persist($userCommercial);
        // -- / COMMERCIALS

        // -- FREELANCES
        $freelance = new Freelance();
        $freelance->setFirstname('Victorien');
        $freelance->setLastname('LEMARIE');
        $freelance->setEmail('victorien@vizlio.fr');
        $freelance->setTel('06 75 48 83 88');
        $freelance->setBirthDate(new \DateTime('1994-06-27'));
        $freelance->setJob('Développeur Web Full Stack trop fort');
        $freelance->setDigitalKnowledge('Imbattable');
        $freelance->setWebUsing('Il a inventé le web.');
        $freelance->setObjective('Révolutionner le monde.');
        $freelance->addPersonSkill($personSkill8);
        $freelance->addPersonSkill($personSkill9);

        $freelance->setUsername('victorien');
        $password = $this->encoder->hashPassword($freelance, 'password');
        $freelance->setPassword($password);
        $freelance->addRole(User::ROLE_FREELANCE);

        $manager->persist($freelance);
        // -- / FREELANCES

        // -- CANDIDATES
        $candidate = new Candidate();
        $candidate->setFirstname('Malvic');
        $candidate->setLastname('LEROUX');
        $candidate->setEmail('malvic@gmail.com');
        $candidate->setTel('06 75 48 83 88');
        $candidate->setBirthDate(new \DateTime('1994-01-19'));
        $candidate->setJob('Data Scientist');
        $candidate->setDigitalKnowledge('Passable');
        $candidate->setWebUsing("Il essaie de s'en sortir");
        $candidate->setObjective('Trouver sa place dans ce monde de brutes.');
        $candidate->addPersonSkill($personSkill10);
        $candidate->addPersonSkill($personSkill11);

        $manager->persist($candidate);

        $candidate = new Candidate();
        $candidate->setFirstname('Théo');
        $candidate->setLastname('FRIGOUT');
        $candidate->setEmail('theo@gmail.com');
        $candidate->setTel('06 99 48 83 88');
        $candidate->setBirthDate(new \DateTime('1999-05-19'));
        $candidate->setJob('Styliste');
        $candidate->setDigitalKnowledge('Perdu');
        $candidate->setWebUsing("Il essaie de s'en sortir");
        $candidate->setObjective('Trouver sa place dans ce monde de brutes.');
        $candidate->addPersonSkill($personSkill12);

        $manager->persist($candidate);

        $candidate = new Candidate();
        $candidate->setFirstname('Mac');
        $candidate->setLastname('MILLER');
        $candidate->setEmail('mac@gmail.com');
        $candidate->setTel('06 30 48 60 88');
        $candidate->setBirthDate(new \DateTime('1998-06-25'));
        $candidate->setJob('Chanteur');
        $candidate->setDigitalKnowledge('Bien');
        $candidate->setWebUsing("Facebook");
        $candidate->setObjective('être libre');
        $candidate->addPersonSkill($personSkill13);
        $candidate->addPersonSkill($personSkill14);

        $manager->persist($candidate);
        // -- / CANDIDATES
        

        // -- COMPANIES
        $company = new Company();
        $company->setName('Hollister');
        $company->setCreationDate(new \DateTime('1985-01-08'));
        $manager->persist($company);

        $company = new Company();
        $company->setName('Lamacompta');
        $company->setCreationDate(new \DateTime('2020-02-14'));
        $manager->persist($company);
        // -- / COMPANIES

        // Flush all
        $manager->flush();
    }
}
