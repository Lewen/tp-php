<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('tel')
            ->add('birthDate', BirthdayType::class, [
                'placeholder' => 'Select a value',
            ])
            ->add('job')
            ->add('digitalKnowledge')
            ->add('webUsing')
            ->add('objective')
            ->add('username')
            ->add('roles', ChoiceType::class, [
                'choices'  => [
                    'Admin' => User::ROLE_ADMIN,
                    'Collaborator' => User::ROLE_COLLABORATOR,
                    'Commercial' => User::ROLE_COMMERCIAL,
                    'Freelance' => User::ROLE_FREELANCE,
                ],
                'multiple' => true
            ])
            ->add('password')
            ->add('personSkills', CollectionType::class, [
                'entry_type' => PersonSkillType::class,
                'entry_options' => ['label' => false],
                'by_reference' => false,
                'allow_add'  => true,
                'allow_delete' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
